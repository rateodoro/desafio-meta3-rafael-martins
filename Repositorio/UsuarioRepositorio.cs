﻿using Modelo;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Repositorio
{
    public static class UsuarioRepositorio
    {
        public static Usuario ObterUsuarioPorLogin(string login)
        {
            using (var conection = Conexao.ConexaoDB.Conectar())
            {
                var sql = Scripts.Usuario.ObterUsuarioPorLogin;

                SqlCommand cmd = new SqlCommand(sql, conection);
                cmd.Parameters.Add("Login", SqlDbType.VarChar).Value = "%" + login + "%";
                Usuario usuario = null;
                var rd = cmd.ExecuteReader();
                while (rd.Read())
                {
                    usuario = new Usuario
                    {
                        IdUsuario = Convert.ToInt32(rd["id"]),
                        IdPessoaFisica = Convert.ToInt32(rd["IdPessoaFisica"]),
                        Nome = Convert.ToString(rd["Nome"]),
                        Login = Convert.ToString(rd["Login"]),
                        Senha = Convert.ToString(rd["Senha"]),
                        DataInclusao = (rd["DataInclusao"] != null) ? Convert.ToDateTime(rd["DataInclusao"]) : DateTime.MinValue,
                        Ativo = Convert.ToBoolean(rd["Ativo"])
                    };
                }


                return usuario;
            }
        }

        public static bool ValidaLogin(string login, string senha)
        {
            using (var conection = Conexao.ConexaoDB.Conectar())
            {
                var sql = Scripts.Usuario.ValidaLogin;

                SqlCommand cmd = new SqlCommand(sql, conection);
                cmd.Parameters.Add("Login", SqlDbType.VarChar).Value = login;
                cmd.Parameters.Add("Senha", SqlDbType.VarChar).Value = senha;
                var rd = Convert.ToInt16(cmd.ExecuteScalar());
                return rd == 1;
            }
        }

        public static List<Usuario> ObterListaUsuario()
        {
            List<Usuario> usuariosLista = new List<Usuario>();

            using (var conection = Conexao.ConexaoDB.Conectar())
            {
                var sql = Scripts.Usuario.ObterListaUsuarios;

                SqlCommand cmd = new SqlCommand(sql, conection);

                using (var reader = cmd.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        usuariosLista.Add(new Usuario
                        {
                            IdUsuario = Convert.ToInt32(reader.GetValue(0)),
                            IdPessoaFisica = Convert.ToInt32((reader.GetValue(1))),
                            Login = Convert.ToString(reader.GetValue(2)),
                            Senha = Convert.ToString(reader.GetValue(3)),
                            DataInclusao = Convert.ToDateTime(reader.GetValue(4)),
                            Ativo = Convert.ToBoolean((reader.GetValue(5))),

                        });
                    }
                }

                return usuariosLista;

            }
        }

        public static Usuario ObterUsuarioPorId(int id)
        {
            using (var conection = Conexao.ConexaoDB.Conectar())
            {
                var sql = Scripts.Usuario.ObterUsuarioPorId;

                SqlCommand cmd = new SqlCommand(sql, conection);
                cmd.Parameters.Add(new SqlParameter { ParameterName = "@IdUsuario", Value = id });
                Usuario usuario = null;
                var rd = cmd.ExecuteReader();
                while (rd.Read())
                {
                    usuario = new Usuario
                    {
                        IdUsuario = Convert.ToInt32(rd["Id"]),
                        IdPessoaFisica = Convert.ToInt32(rd["IdPessoaFisica"]),
                        Login = Convert.ToString(rd["Login"]),
                        Senha = Convert.ToString(rd["Senha"]),
                        DataInclusao = (rd["DataInclusao"] != null) ? Convert.ToDateTime(rd["DataInclusao"]) : DateTime.MinValue,
                        Ativo = Convert.ToBoolean(rd["Ativo"]),

                    };
                }

                return usuario;
            }
        }

        public static void AtualizarUsuario(Usuario usuario)
        {
            try
            {
                using (var conection = Conexao.ConexaoDB.Conectar())
                {
                    var sql = Scripts.Usuario.AtualizarUsuario;
                    SqlCommand cmd = new SqlCommand(sql, conection);

                    cmd.Parameters.AddWithValue("@IdPessoaFisica", usuario.IdPessoaFisica);
                    cmd.Parameters.AddWithValue("@IdUsuario", usuario.IdUsuario);
                    cmd.Parameters.AddWithValue("@Login", usuario.Login);
                    cmd.Parameters.AddWithValue("@Senha", usuario.Senha);
                    cmd.Parameters.AddWithValue("@DataInclusao", usuario.DataInclusao);
                    cmd.Parameters.AddWithValue("@Ativo", usuario.Ativo);

                    cmd.ExecuteNonQuery();

                }
            }
            catch (Exception e)
            {

            }
        }

        public static void DeletarUsuario(int id)
        {
            try
            {
                using (var conection = Conexao.ConexaoDB.Conectar())
                {
                    var sql = Repositorio.Scripts.Usuario.DeletarUsuario;
                    SqlCommand cmd = new SqlCommand(sql, conection);
                    cmd.Parameters.AddWithValue(@"IdUsuario", id);
                    cmd.ExecuteNonQuery();
                }
            }
            catch
            {

            }

        }

        public static void InserirUsuario(Usuario usuario)
        {
            try
            {
                using (var conection = Conexao.ConexaoDB.Conectar())
                {
                    var sql = Scripts.Usuario.InserirUsuario;
                    SqlCommand cmd = new SqlCommand(sql, conection);

                    cmd.Parameters.AddWithValue("@Login", usuario.Login);
                    cmd.Parameters.AddWithValue("@Senha", usuario.Senha);
                    cmd.Parameters.AddWithValue("@DataInclusao", usuario.DataInclusao);
                    cmd.Parameters.AddWithValue("@Ativo", usuario.Ativo);

                    cmd.ExecuteNonQuery();

                }
            }
            catch (Exception e)
            {

            }
        }
        
    }
}
